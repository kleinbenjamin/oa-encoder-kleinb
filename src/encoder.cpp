#include "encoder.h"


bool Encoder::successfullyReadJsonFile(QString path) {
    bool goodToGo = true;

    if(path.isEmpty()) {
        goodToGo = false;
        qDebug() << "ERROR: invalid coding table\n";
    } else {
        QFile file;
        file.setFileName(path);

        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            file.close();
            goodToGo = false;
            qDebug() << "ERROR: invalid coding table\n";
        } else {
            QString str = file.readAll();
            file.close();

            QJsonDocument d = QJsonDocument::fromJson(str.toUtf8());
            QJsonObject jsonObj = d.object();

            foreach(const QString& key, jsonObj.keys()) {
                QJsonValue value = jsonObj.value(key);
                if(!value.isString()) {
                    goodToGo = false;
                    mapOfEncoder.clear();
                    mapOfDecoder.clear();
                    qDebug() << "ERROR: invalid coding table\n";
                    break;
                } else {
                    mapOfEncoder.emplace(key, value.toString());
                    mapOfDecoder.emplace(value.toString(), key);
                }
            }
        }
    }

    return goodToGo;
   }

QString Encoder::encodeParam(QString param) {
    QString answer;
    for(const auto& c : param) {
        if(mapOfEncoder.count(c) == 1) {
            answer += mapOfEncoder[c];
        } else {
            answer = INVALID_INPUT;
            break;
        }
    }

    return answer;
}

QString Encoder::encodeList(QStringList list) {
    QString translation;

    for(int i = 0; i < list.length(); ++i) {
        QString answer = encodeParam(list[i]);
        if(answer == INVALID_INPUT) {
            translation = INVALID_INPUT;
            break;
        } else {
            translation += answer + " ";
        }
    }

    return translation;
}

QString Encoder::decodeParam(QString param) {
    QString answer;
    int loops = 0;

    while(param.length() > 0 && loops < 2) {
        for(const auto& row: mapOfDecoder) {
            if(param.startsWith(row.first)) {
                answer += row.second;
                param = param.remove(0, row.first.length());
                loops = 0;
            }
        }
        ++loops;
    }

    return loops < 2 ? answer : INVALID_INPUT;
}

QString Encoder::decodeList(QStringList list) {
    QString translation;

    for(int i = 0; i < list.length(); ++i) {
        QString answer = decodeParam(list[i]);
        if(answer == INVALID_INPUT) {
            translation = INVALID_INPUT;
            break;
        } else {
            translation += answer + " ";
        }
    }

    return translation;
}

void Encoder::encodeFile(QStringList paths) {
    if(paths.length() < 2 || paths.at(0).isEmpty() || paths.at(1).isEmpty()) {
        std::cout << "MISSING INPUT OR OUTPUT FILE";
    } else {
        QFile fromFile(paths.at(0));
        QFile toFile(paths.at(1));
        if ((!fromFile.open(QIODevice::ReadOnly | QIODevice::Text)) || (!toFile.open(QIODevice::WriteOnly | QIODevice::Text))) {
            fromFile.close();
            toFile.close();
            std::cout << "INVALID INPUT OR OUPUT FILE";
        } else {
            QTextStream in(&fromFile);
            QTextStream out(&toFile);
            QString answer;
            while (!in.atEnd()) {
                QString line = in.readLine();
                QStringList list = line.split(' ');
                answer += encodeList(list) + "\n";
            }
            out  << answer;
            fromFile.close();
            toFile.close();
          std::cout << answer.toStdString() << std::endl;
        }
    }
}

void Encoder::decodeFile(QStringList paths) {
    if(paths.length() < 2 || paths.at(0).isEmpty() || paths.at(1).isEmpty()) {
        std::cout << "MISSING INPUT OR OUTPUT FILE";
    } else {
        QFile fromFile(paths.at(0));
        QFile toFile(paths.at(1));
        if ((!fromFile.open(QIODevice::ReadOnly | QIODevice::Text)) || (!toFile.open(QIODevice::WriteOnly | QIODevice::Text))) {
            fromFile.close();
            toFile.close();
            std::cout << "INVALID INPUT OR OUPUT FILE";
        } else {
            QTextStream in(&fromFile);
            QTextStream out(&toFile);
            QString answer;
            while (!in.atEnd()) {
                QString line = in.readLine();
                QStringList list = line.split(' ');
                answer += decodeList(list) + "\n";
            }
            out  << answer;
            fromFile.close();
            toFile.close();
            std::cout << answer.toStdString() << std::endl;
        }
    }
}
