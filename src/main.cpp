#include <iostream>
#include <QTextStream>
#include "encoder.h"


int main() {
    Encoder encoder;

    bool successful = false;
    while(!successful) {
        std::cout << "Path to JSON file: ";
        QTextStream s(stdin);
        QString path = s.readLine();
        successful = encoder.successfullyReadJsonFile(path);
    }


    while (true) {
        std::cout << "ENCODE param1 param2 ...\n"
                     "DECODE param1 param2 ...\n"
                     "ENCODE_FILE file/to/encode file/to/write/output\n"
                     "DECODE_FILE file/to/decode file/to/write/output\n"
                     "EXIT\n\n";

        QTextStream s(stdin);
        QString command = s.readLine();

        QStringList listFromCommand = command.split(' ');
        QString c = listFromCommand.first().toUpper();
        listFromCommand.removeFirst();

        if(c == "ENCODE") {
            qDebug() << encoder.encodeList(listFromCommand) << endl;
        } else if(c == "DECODE") {
            qDebug() << encoder.decodeList(listFromCommand) << endl;
        } else if(c == "ENCODE_FILE") {
            encoder.encodeFile(listFromCommand);
        } else if(c == "DECODE_FILE") {
            encoder.decodeFile(listFromCommand);
        } else if(c == "EXIT") {
            std::cout << "KTHXBYE";
            break;
        } else {
            std::cout << "Invalid command!\n";
        }
    }

    return 0;
}
