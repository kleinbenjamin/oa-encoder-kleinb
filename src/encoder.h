#include <iostream>
#include <map>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QDebug>

class Encoder {
private:
    const QString INVALID_INPUT = "INVALID_INPUT";
    std::map<QString, QString> mapOfEncoder;
    std::map<QString, QString> mapOfDecoder;
public:
    bool successfullyReadJsonFile(QString path);
    QString encodeParam(QString param);
    QString encodeList(QStringList list);
    QString decodeParam(QString param);
    QString decodeList(QStringList list);
    void encodeFile(QStringList paths);
    void decodeFile(QStringList paths);
};
